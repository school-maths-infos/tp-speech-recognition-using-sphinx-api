package jc.info007.sphinxtp;

import java.io.*;
import java.util.List;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.api.StreamSpeechRecognizer;
import edu.cmu.sphinx.result.WordResult;

import jc.info007.sphinxtp.model.Pacman;
import jc.info007.sphinxtp.view.GridFrame;

public class Main {

    public static void main(String[] args) {

        Pacman pacman = new Pacman(0,0);
        GridFrame frame = new GridFrame(pacman);


        Configuration configuration = new Configuration();

        configuration
                .setAcousticModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us");
        configuration
                .setDictionaryPath("resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict");
        configuration
                .setLanguageModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us.lm.bin");
        configuration.setGrammarPath("resource:/example/");
        configuration.setGrammarName("hello");
        configuration.setUseGrammar(true);

        LiveSpeechRecognizer recognizer = null;
        try {
            recognizer = new LiveSpeechRecognizer(configuration);
        } catch (IOException e) {
            e.printStackTrace();
        }


        recognize(pacman, recognizer, frame);

    }

    private static void recognize(Pacman pacman, LiveSpeechRecognizer recognizer, GridFrame gridFrame) {

        recognizer.startRecognition(true);
        SpeechResult result;

        while ((result = recognizer.getResult()) != null) {
            String sentence = result.getHypothesis().toString();
            if (sentence.equals("turn to the left")) {
                pacman.goLeft();
            } else if (sentence.equals("turn to the right")) {
                pacman.goRight();
            } else if (sentence.equals("go to the top")) {
                pacman.goTop();
            } else if (sentence.equals("go to the bottom")) {
                pacman.goDown();
            } else {
                System.out.println("invalid sentence : " + sentence);
            }
            gridFrame.updateFrame();
        }
        recognizer.stopRecognition();
    }

}
