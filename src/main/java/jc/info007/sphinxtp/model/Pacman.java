package jc.info007.sphinxtp.model;

import jc.info007.sphinxtp.Constant;

public class Pacman {

    private int posX;
    private int posY;

    public Pacman(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public void goLeft() {
        System.out.println("jc.info007.sphinxtp.model.Pacman.goLeft");
        this.setPosY((this.getPosY() - 1 + Constant.WIDTH) % Constant.WIDTH);

    }

    public void goRight() {
        System.out.println("jc.info007.sphinxtp.model.Pacman.goRight");
        this.setPosY((this.getPosY() + 1) % Constant.WIDTH);
    }

    public void goTop() {
        System.out.println("jc.info007.sphinxtp.model.Pacman.goTop");
        this.setPosX((this.getPosX() - 1 + Constant.HEIGHT) % Constant.HEIGHT);
    }

    public void goDown() {
        System.out.println("jc.info007.sphinxtp.model.Pacman.goDown");
        this.setPosX((this.getPosX() + 1) % Constant.HEIGHT);
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }
}
