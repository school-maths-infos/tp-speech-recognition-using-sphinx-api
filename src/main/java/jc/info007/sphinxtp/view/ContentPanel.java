package jc.info007.sphinxtp.view;

import javax.swing.*;
import java.awt.*;

public class ContentPanel extends JPanel {

    public ContentPanel() {

        super();
        this.setBackground(Color.WHITE);
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
    }

    public ContentPanel(Color color) {

        super();
        this.setBackground(color);
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
    }
}
