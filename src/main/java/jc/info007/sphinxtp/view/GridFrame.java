package jc.info007.sphinxtp.view;

import jc.info007.sphinxtp.Constant;
import jc.info007.sphinxtp.model.Pacman;

import javax.swing.*;
import java.awt.*;

public class GridFrame extends JFrame {

    private Pacman pacman;

    public GridFrame(Pacman pacman) {

        this.pacman = pacman;
        GridLayout matrice = new GridLayout(Constant.HEIGHT, Constant.WIDTH);
        this.getContentPane().setLayout(matrice);
        this.drawGrid();

        this.setSize(new Dimension(Constant.WIDTH * 10, Constant.HEIGHT * 10));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void updateFrame() {

        this.getContentPane().removeAll();

        this.drawGrid();
        this.revalidate();
    }

    private void drawGrid() {

        for(int i = 0; i < Constant.HEIGHT; i++) {
            for(int j = 0; j < Constant.WIDTH; j++) {
                if (pacman.getPosX() == i && pacman.getPosY() == j) {
                    this.getContentPane().add(new ContentPanel(Color.BLUE));
                } else {
                    this.getContentPane().add(new ContentPanel());
                }
            }
        }
    }


}
